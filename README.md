# OpenML dataset: Breast

https://www.openml.org/d/45085

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Breast Cancer dataset**

**Authors**: L. van't Veer, H. Dai, M. Van De Vijver, Y. He, A. Hart, M. Mao, H. Peterse, K. van der Kooy, M. Marton, A. Witteveen, et al

**Please cite**: ([URL](https://www.nature.com/articles/415530a)): L. van't Veer, H. Dai, M. Van De Vijver, Y. He, A. Hart, M. Mao, H. Peterse, K. van der Kooy, M. Marton, A. Witteveen, et al, Gene expression profiling predicts clinical outcome of breast cancer, Nature 415 (6871) (2002) 530-536

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45085) of an [OpenML dataset](https://www.openml.org/d/45085). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45085/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45085/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45085/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

